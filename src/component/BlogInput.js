import React, { Component } from "react";

const initialState = {
  articlename: "",
  articlebody: "",
  articleauther: "",
  nameError: "",
  bodyError: "",
  autherError: "",
 
};

export default class BlogInput extends Component {
  state = initialState;

  validation = () => {
    let nameError = "";
    let bodyError = "";
    let autherError = "";

    if (!this.state.articlename) {
      nameError = "name canot empty";
    }
    if (!this.state.articlebody) {
      bodyError = "body canot empty";
    }
    if (!this.state.articleauther) {
      autherError = "auther field canot empty";
    }

    if (nameError || bodyError || autherError) {
      this.setState({ nameError, bodyError, autherError });
      return false;
    } 
    return true;
  };
  

  textChange = (e) => {
   let value= this.setState({ [e.target.name]: e.target.value });

    
  };
  submit = (e) => {
    e.preventDefault();
    const isValid = this.validation();
    if (isValid) {
      console.log(this.state);
    
    }
   
  };

  render() {

    
    return (
      <div className="p-3 mb-2 bg-light text-dark">
        <div className="col-md-4 offset-md-4">
          <form id="create-course-form">
            <h2 className="text-center">LightBlog</h2>
            <div className="form-group">
              <label></label>
              <input
                onChange={this.textChange}
                value={this.state.article_name}
                type="text"
                className="form-control"
                name="articlename"
                placeholder="Article Title"
              />
              <div style={{ fontSize: 12, color: "red" }}>
                {this.state.nameError}
              </div>
            </div>
            <div className="form-group">
              <label for="exampleFormControlTextarea1"></label>
              <textarea
                onChange={this.textChange}
                className="form-control"
                placeholder="Article Body"
                name="articlebody"
                rows="3"
              ></textarea>
              <div style={{ fontSize: 12, color: "red" }}>
                {this.state.bodyError}
              </div>
            </div>
            <div className="form-group">
              <label></label>
              <input
                onChange={this.textChange}
                type="text area"
                className="form-control"
                name="articleauther"
                placeholder="Article Auther"
              />
              <div style={{ fontSize: 12, color: "red" }}>
                {this.state.autherError}
              </div>
            </div>

            <button
              onClick={this.submit}
              type="submit"
              className="btn btn-primary btn-lg float-right btn-sm"
            >
              Submit
            </button>
          </form>
        </div>
        
      </div>
    );
  }
}
