import React from 'react';

import './App.css';
import BlogInput from './component/BlogInput';

function App() {
  return (
   <div>
     <BlogInput />
    </div>
  );
}

export default App;
